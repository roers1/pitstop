using WebApp.Models;

namespace WebApp.ViewModels
{
    public class InventoryManagementNewViewModel
    {
        public Product Product { get; set; }
    }
}
