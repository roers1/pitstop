﻿using WebApp.Models;

namespace WebApp.ViewModels
{
    public class InventoryManagementDetailsViewModel
    {
        public Product Product { get; set; }
    }
}
