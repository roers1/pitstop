﻿using System.Collections.Generic;
using WebApp.Models;

namespace WebApp.ViewModels
{
    public class InventoryManagementViewModel
    {
        public IEnumerable<Product> Products { get; set; }
    }
}
