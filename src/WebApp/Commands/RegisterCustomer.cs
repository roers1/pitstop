﻿using Pitstop.Infrastructure.Messaging;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Commands
{
    public class RegisterProduct : Command
    {
        public readonly string ProductId;
        public readonly string Name;
        public readonly decimal Price;

        public RegisterProduct(Guid messageId, string productId, string name, decimal price) : base(messageId)
        {
	        ProductId = productId;
            Name = name;
            Price = price;
        }
    }
}
