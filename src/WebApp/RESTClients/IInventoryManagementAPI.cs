﻿using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApp.Commands;
using WebApp.Models;

namespace WebApp.RESTClients
{
    public interface IInventoryManagementAPI
    {
        [Get("/products")]
        Task<List<Product>> GetProducts();

        [Get("/products/{id}")]
        Task<Product> GetProductById([AliasAs("id")] string productId);

        [Post("/products")]
        Task RegisterProduct(RegisterProduct command);
    }
}
