﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Refit;
using WebApp.Commands;
using System.Net;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System;
using WebApp.Models;

namespace WebApp.RESTClients
{
    public class InventoryManagementAPI : IInventoryManagementAPI
    {
        private IInventoryManagementAPI _restClient;

        public  InventoryManagementAPI(IConfiguration config, HttpClient httpClient)
        {
            string apiHostAndPort = config.GetSection("APIServiceLocations").GetValue<string>("InventoryManagementAPI");
            httpClient.BaseAddress = new Uri($"http://{apiHostAndPort}/api");
            _restClient = RestService.For<IInventoryManagementAPI>(httpClient);
        }

        public async Task<List<Product>> GetProducts()
        {
            return await _restClient.GetProducts();
        }

        public async Task<Product> GetProductById([AliasAs("id")] string productId)
        {
            try
            {
                return await _restClient.GetProductById(productId);
            }
            catch (ApiException ex)
            {
                if (ex.StatusCode == HttpStatusCode.NotFound)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }
        }

        public async Task RegisterProduct(RegisterProduct command)
        {
            await _restClient.RegisterProduct(command);
        }
    }
}
