﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pitstop.Infrastructure.Messaging;

namespace Pitstop.WorkshopManagementEventHandler.Events
{
	public class ProductRegistered : Event
	{
		public readonly string ProductId;
		public readonly string Name;
		public readonly decimal Price;
		public ProductRegistered(Guid messageId, string productId, string name, decimal price) : base(messageId)
		{
			ProductId = productId;
			Name = name;
			Price = price;
		}
	}
}
