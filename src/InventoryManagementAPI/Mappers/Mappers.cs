﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryManagementAPI.Commands;
using InventoryManagementAPI.Events;
using InventoryManagementAPI.Model;

namespace InventoryManagementAPI.Mappers
{
	public static class Mappers
	{
		public static ProductRegistered MapToProductRegistered(this RegisterProduct command) => new ProductRegistered
		(
			System.Guid.NewGuid(),
			command.ProductId,
			command.Name,
			command.Price
		);

		public static Product MapToProduct(this RegisterProduct command) => new Product
		{
			ProductId = command.ProductId,
			Name = command.Name,
			Price = command.Price
		};
    }
}