﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryManagementAPI.Model
{
	public class Product
	{
		public string ProductId { get; set; }
		public string Name { get; set; }
		public decimal Price { get; set; }

    }
}
