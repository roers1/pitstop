﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Polly;
using System;
using InventoryManagementAPI.Model;


namespace InventoryManagementAPI.DataAccess
{
	public class InventoryManagementDBContext : DbContext
	{
		public InventoryManagementDBContext(DbContextOptions<InventoryManagementDBContext> options) : base(options)
		{

		}

		public DbSet<Product> Products { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			builder.Entity<Product>().HasKey(m => m.ProductId);
			builder.Entity<Product>().ToTable("Product");
			base.OnModelCreating(builder);
		}

		public void MigrateDB()
		{
			Policy
				.Handle<Exception>()
				.WaitAndRetry(10, r => TimeSpan.FromSeconds(10))
				.Execute(() => Database.Migrate());
		}
	}
}
