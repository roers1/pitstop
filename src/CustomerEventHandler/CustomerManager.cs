﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CustomerEventHandler.Events;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Linq;
using Pitstop.Infrastructure.Messaging;
using Serilog;

namespace Pitstop.CustomerEventHandler
{
	class CustomerManager : IHostedService, IMessageHandlerCallback
	{
		public IMessageHandler _messageHandler { get; set; }

		public CustomerManager(IMessageHandler messageHandler)
		{
			_messageHandler = messageHandler;
		}

		public Task StartAsync(CancellationToken cancellationToken)
		{
			_messageHandler.Start(this);
			return Task.CompletedTask;
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			_messageHandler.Stop();
			return Task.CompletedTask;
		}

		public async Task<bool> HandleMessageAsync(string messageType, string message)
		{
			JObject messageObject = MessageSerializer.Deserialize(message);
			Console.Write(messageObject);
			try
			{
				switch (messageType)
				{
					case "CustomerRegistered":
						await HandleAsync(messageObject.ToObject<CustomerRegistered>());
						break;
				}
			}
			catch (Exception ex)
			{
				string messageId = messageObject.Property("MessageId") != null ? messageObject.Property("MessageId").Value<string>() : "[unknown]";
				Log.Error(ex, "Error while handling {MessageType} message with id {MessageId}.", messageType, messageId);
			}

			// always akcnowledge message - any errors need to be dealt with locally.
			return true;
		}

		private async Task<bool> HandleAsync(CustomerRegistered e)
		{

			Console.Write(e.name);

			return true;
		}
	}
}
