﻿using Pitstop.Infrastructure.Messaging;

namespace CustomerEventHandler.Events
{
	class CustomerRegistered : Event
	{

		public string customerId { get; set; }
		public string name { get; set; }

		 public CustomerRegistered(string customerId, string name) : base()
        {
            this.customerId = customerId;
            this.name = name;
        }

	}
}
