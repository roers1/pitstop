﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Pitstop.WorkshopManagementAPI.Repositories;

namespace Pitstop.WorkshopManagementAPI.Controllers
{
    [Route("/api/[controller]")]
    public class RefDataController : Controller
    {
        ICustomerRepository _customerRepo;
        IVehicleRepository _vehicleRepo;
        IInventoryRepository _inventoryRepo;

        public RefDataController(ICustomerRepository customerRepo, IVehicleRepository vehicleRepo, IInventoryRepository inventoryRepo)
        {
            _customerRepo = customerRepo;
            _vehicleRepo = vehicleRepo;
            _inventoryRepo = inventoryRepo;
        }

        [HttpGet]
        [Route("customers")]
        public async Task<IActionResult> GetCustomers()
        {
            return Ok(await _customerRepo.GetCustomersAsync());
        }

        [HttpGet]
        [Route("customers/{customerId}")]
        public async Task<IActionResult> GetCustomerByCustomerId(string customerId)
        {
            var customer = await _customerRepo.GetCustomerAsync(customerId);
            if (customer == null)
            {
                return NotFound();
            }
            return Ok(customer);
        }

        [HttpGet]
        [Route("products")]
        public async Task<IActionResult> GetProducts()
        {
	        return Ok(await _inventoryRepo.GetProductsAsync());
        }

        [HttpGet]
        [Route("products/{productId}")]
        public async Task<IActionResult> GetCustomerByProductId(string productId)
        {
	        var product = await _inventoryRepo.GetProductAsync(productId);
	        if (product == null)
	        {
		        return NotFound();
	        }
	        return Ok(product);
        }

        [HttpGet]
        [Route("vehicles")]
        public async Task<IActionResult> GetVehicles()
        {
            return Ok(await _vehicleRepo.GetVehiclesAsync());
        }

        [HttpGet]
        [Route("vehicles/{licenseNumber}")]
        public async Task<IActionResult> GetVehicleByLicenseNumber(string licenseNumber)
        {
            var vehicle = await _vehicleRepo.GetVehicleAsync(licenseNumber);
            if (vehicle == null)
            {
                return NotFound();
            }
            return Ok(vehicle);
        }
    }
}
