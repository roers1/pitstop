﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pitstop.WorkshopManagementAPI.Repositories.Model;

namespace Pitstop.WorkshopManagementAPI.Repositories
{
	public interface IInventoryRepository
	{
		Task<IEnumerable<Product>> GetProductsAsync();
		Task<Product> GetProductAsync(string productId);
	}
}
